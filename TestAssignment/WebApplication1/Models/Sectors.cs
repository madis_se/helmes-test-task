﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
  

    public class Sectors
    {
        public int ID { get; set; }

        public int SectorsID { get; set; }

        [Display(Name = "Name")]
        public string SectorNames { get; set; }

        public int SectorLvl { get; set; }

    }
}
