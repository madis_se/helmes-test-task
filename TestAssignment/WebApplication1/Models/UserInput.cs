﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApplication1.Models
{
  

    public class UserInput
    {
        public int ID { get; set; }

        [Display(Name = "Sector id")]
        [Required]
        public int SectorsID { get; set; }


       // public List<SelectListItem> SectorList { get; set; }
        
        [Display(Name = "Name")]
        [RegularExpression(@"^[a-zA-Z""'\s-]*$")]
        [StringLength(30, MinimumLength = 3)]   
        [Required]
        public string Name { get; set; }
        
        [Required]
        public bool Terms { get; set; }

        public string SessionID { get; set; }

       
    }

   
    }
