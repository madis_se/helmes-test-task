﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

using WebApplication1.AspNetCore.NewDb.Models;
namespace WebApplication1.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new Model.MainContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<Model.MainContext>>()))
            {

                if (context.Sectors.Any())
                {
                    return;   // DB has been seeded
                }

                context.Sectors.AddRange(
 new Sectors
 {
     SectorsID = 19,
     SectorNames = "Construction materials",
     SectorLvl = 1,

 },
 new Sectors
 {
     SectorsID = 18,
     SectorNames = "Electronics and Optics",
     SectorLvl = 1,

 },
 new Sectors
 {
     SectorsID = 6,
     SectorNames = "Food and Beverage",
     SectorLvl = 1,

 },
 new Sectors
 {
     SectorsID = 342,
     SectorNames = "Bakery & confectionery products",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 43,
     SectorNames = "Beverages",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 42,
     SectorNames = "Fish & fish products ",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 40,
     SectorNames = "Meat & meat products",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 39,
     SectorNames = "Milk & dairy products ",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 437,
     SectorNames = "Other",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 378,
     SectorNames = "Sweets & snack food",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 13,
     SectorNames = "Furniture",
     SectorLvl = 1,

 },
 new Sectors
 {
     SectorsID = 389,
     SectorNames = "Bathroom/sauna ",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 385,
     SectorNames = "Bedroom",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 390,
     SectorNames = "Children’s room ",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 98,
     SectorNames = "Kitchen ",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 101,
     SectorNames = "Living room ",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 392,
     SectorNames = "Office",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 394,
     SectorNames = "Other (Furniture)",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 341,
     SectorNames = "Outdoor ",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 99,
     SectorNames = "Project furniture",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 12,
     SectorNames = "Machinery",
     SectorLvl = 1,

 },
 new Sectors
 {
     SectorsID = 94,
     SectorNames = "Machinery components",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 91,
     SectorNames = "Machinery equipment/tools",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 224,
     SectorNames = "Manufacture of machinery ",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 97,
     SectorNames = "Maritime",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 271,
     SectorNames = "Aluminium and steel workboats ",
     SectorLvl = 3,

 },
 new Sectors
 {
     SectorsID = 269,
     SectorNames = "Boat/Yacht building",
     SectorLvl = 3,

 },
 new Sectors
 {
     SectorsID = 230,
     SectorNames = "Ship repair and conversion",
     SectorLvl = 3,

 },
 new Sectors
 {
     SectorsID = 93,
     SectorNames = "Metal structures",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 508,
     SectorNames = "Other",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 227,
     SectorNames = "Repair and maintenance service",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 11,
     SectorNames = "Metalworking",
     SectorLvl = 1,

 },
 new Sectors
 {
     SectorsID = 67,
     SectorNames = "Construction of metal structures",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 263,
     SectorNames = "Houses and buildings",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 267,
     SectorNames = "Metal products",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 542,
     SectorNames = "Metal works",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 75,
     SectorNames = "CNC-machining",
     SectorLvl = 3,

 },
 new Sectors
 {
     SectorsID = 62,
     SectorNames = "Forgings, Fasteners ",
     SectorLvl = 3,

 },
 new Sectors
 {
     SectorsID = 69,
     SectorNames = "Gas, Plasma, Laser cutting",
     SectorLvl = 3,

 },
 new Sectors
 {
     SectorsID = 66,
     SectorNames = "MIG, TIG, Aluminum welding",
     SectorLvl = 3,

 },
 new Sectors
 {
     SectorsID = 9,
     SectorNames = "Plastic and Rubber",
     SectorLvl = 1,

 },
 new Sectors
 {
     SectorsID = 54,
     SectorNames = "Packaging",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 556,
     SectorNames = "Plastic goods",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 559,
     SectorNames = "Plastic processing technology",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 55,
     SectorNames = "Blowing",
     SectorLvl = 3,

 },
 new Sectors
 {
     SectorsID = 57,
     SectorNames = "Moulding",
     SectorLvl = 3,

 },
 new Sectors
 {
     SectorsID = 53,
     SectorNames = "Plastics welding and processing",
     SectorLvl = 3,

 },
 new Sectors
 {
     SectorsID = 560,
     SectorNames = "Plastic profiles",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 5,
     SectorNames = "Printing ",
     SectorLvl = 1,

 },
 new Sectors
 {
     SectorsID = 148,
     SectorNames = "Advertising",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 150,
     SectorNames = "Book/Periodicals printing",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 145,
     SectorNames = "Labelling and packaging printing",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 7,
     SectorNames = "Textile and Clothing",
     SectorLvl = 1,

 },
 new Sectors
 {
     SectorsID = 44,
     SectorNames = "Clothing",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 45,
     SectorNames = "Textile",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 8,
     SectorNames = "Wood",
     SectorLvl = 1,

 },
 new Sectors
 {
     SectorsID = 337,
     SectorNames = "Other (Wood)",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 51,
     SectorNames = "Wooden building materials",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 47,
     SectorNames = "Wooden houses",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 3,
     SectorNames = "Other",
     SectorLvl = 0,

 },
 new Sectors
 {
     SectorsID = 37,
     SectorNames = "Creative industries",
     SectorLvl = 1,

 },
 new Sectors
 {
     SectorsID = 29,
     SectorNames = "Energy technology",
     SectorLvl = 1,

 },
 new Sectors
 {
     SectorsID = 33,
     SectorNames = "Environment",
     SectorLvl = 1,

 },
 new Sectors
 {
     SectorsID = 2,
     SectorNames = "Service",
     SectorLvl = 0,

 },
 new Sectors
 {
     SectorsID = 25,
     SectorNames = "Business services",
     SectorLvl = 1,

 },
 new Sectors
 {
     SectorsID = 35,
     SectorNames = "Engineering",
     SectorLvl = 1,

 },
 new Sectors
 {
     SectorsID = 28,
     SectorNames = "Information Technology and Telecommunications",
     SectorLvl = 1,

 },
 new Sectors
 {
     SectorsID = 581,
     SectorNames = "Data processing, Web portals, E-marketing",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 576,
     SectorNames = "Programming, Consultancy",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 121,
     SectorNames = "Software, Hardware",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 122,
     SectorNames = "Telecommunications",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 22,
     SectorNames = "Tourism",
     SectorLvl = 1,

 },
 new Sectors
 {
     SectorsID = 141,
     SectorNames = "Translation services",
     SectorLvl = 1,

 },
 new Sectors
 {
     SectorsID = 21,
     SectorNames = "Transport and Logistics",
     SectorLvl = 1,

 },
 new Sectors
 {
     SectorsID = 111,
     SectorNames = "Air",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 114,
     SectorNames = "Rail",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 112,
     SectorNames = "Road",
     SectorLvl = 2,

 },
 new Sectors
 {
     SectorsID = 113,
     SectorNames = "Water",
     SectorLvl = 2,

 }


                );
                context.SaveChanges();

            }
        }
    }
}
