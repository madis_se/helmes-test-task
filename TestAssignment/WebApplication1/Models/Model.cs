﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;


namespace WebApplication1.AspNetCore.NewDb.Models
{
    public class Model
    {
        public class MainContext : DbContext
        {
            public MainContext(DbContextOptions<MainContext> options)
                : base(options)
            { }

            public DbSet<WebApplication1.Models.Sectors> Sectors { get; set; }
            public DbSet<WebApplication1.Models.UserInput> UserInput { get; set; }
           
           
        }
    }
}
